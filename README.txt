Une fois les éléments précisés dans le document requirements installés, il suffit pour jouer à clickletic d'executer le programme(qui se nomme code_source_clickletic.py)

Une fenêtre va alors apparaitre contenant le jeu.

Il suffira alors de clicker sur la mascotte (qui est en réalité un bouton) au centre de l'interface pour commencer à gagner des mascottes, la monnaie du jeu.
Lorsque vous en aurez accumulées suffisament (le nombre de mascotte est marqué tout en haut) pour acheter votre première amélioration "Rugby" (le prix est de 15 mascottes), vous commencerez à gagner passivement des mascottes.
si vous arrivez à réunir un plus grand nombre de mascottes, vous pourrez acheter des amélioration avec un cout plus important qui vous donnera un plus garnd nombre de mascotte /secondes.

Pour avoir une idée de votre progression dans clickletic, il y a la partie "Statistique"  qui vous donnera vos statistiques de votre partie.

L'objectif est d'avoir le plus de mascottes possibles donc n'arrêtez pas de clicker!!

Pour tester plus facilement le jeu, nous avons ajouter un mode "Admin" qui permet d'avoir un très grand nombre de mascotte rapidement afin de pouvoir acheter toutes les améliorations présentes dans le jeu.
Pour l'activer, il suffit , dans la partie menu de l'interface, de rentrer dans le barre de saisie le mot de passe suivant : Paris 2024. Il faut ensuite appuyer sur le bouton Valider. 
Si le mot de passe est correct, cliquer sur le bouton en dessous de la barre de saisie vous donnera 1 milliard de mascotte. 