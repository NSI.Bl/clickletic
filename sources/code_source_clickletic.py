from tkinter import*
### J'initialise mes variables globales
ressource=0 # represente la monaie du jeu
ressource_augment=0 # represente le gain par seconde 
n_upgrade=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0] #tableau stockant le nombre de chaque amélioration achetée 
boucle_inf=True #définit si oui ou non la boucle infinie loop est active
upgrade_cost=[15, 100, 500, 3000, 10000, 50000, 200000, 1500000, 30609012, 123456789] #tableau stockant le coût de chaque amélioration
total_ressource=0 #represente le nombre total de ressource généré depuis le début de la partie
nb_amelioration=0 #represente le nombre total d'amélioration qui ont été acheté.
total_click=0 #represente le nombre total de clicks depuis le début de la partie
admin=False #definit si oui ou non le mmode Admin est actif.
def loop():
    """ fonction qui crée la boucle infinie  qui fait augmenter la variable ressources du montant de la variable ressource_augment chaque seconde"""
    global ressource
    global ressource_augment
    global total_ressource
    if boucle_inf==True:
        ressource+=ressource_augment
        total_ressource+=ressource_augment
        nb_mascotte.config(text=str(ressource))
        ressource_total.config(text=("Mascottes totales: "+str(total_ressource)))
    main_window.after(1000, loop)


def start_stop():
    """ fonction qui stoppe la boucle infinie loop si elle est active et qui la remet en marche si elle est arrêter """
    global boucle_inf
    if boucle_inf==True:
        boucle_inf=False
        pause_play.config(text="Restart")
    elif boucle_inf==False:
        boucle_inf=True
        pause_play.config(text="Mettre en pause")



def click():
    """ fonction permetant que lorsque que boucle_inf'on appuis sur le bouton "bouton_clicker" on ajoute 1+10% de la variable ressource_augment à la variable ressource """
    global ressource
    global ressource_augment
    global total_ressource
    global total_click
    ressource+=1+int((0.10*ressource_augment))
    total_ressource+=1+int((0.10*ressource_augment))
    total_click+=1
    ressource_total.config(text=("Mascottes totales: "+str(total_ressource)))
    nb_mascotte.config(text=str(ressource))
    stat_click.config(text=("Nombre de clicks: "+str(total_click)))

def buy_upgrade(n): #n represente le numéro de boucle_inf'amélioration concerné par boucle_inf'achat.
    """ fonction permetant d'effectuer un achat d'amélioration"""
    global ressource_augment
    global n_upgrade
    global nb_amelioration
    upgrade_power=[1, 4, 15, 50, 150, 350, 1000, 7500, 50000, 1000000] #represente la valeur ajouté à la variable ressource_augment pour chaque amélioration.
    if cost(n)==True:
        ressource_augment+=upgrade_power[n]
        nb_mascotte_sec.config(text=str(ressource_augment))
        n_upgrade[n]+=1
        nb_amelioration+=1
        stat_amélioration.config(text=("Nombre total d'améliorations: "+ str(nb_amelioration)))
        if n==0:
            up_compt_sport1.config(text=str(n_upgrade[n])) #permet d'actualiser la valeur afficher par le Label up_compt_sport1.
        elif n==1:
            up_compt_sport2.config(text=str(n_upgrade[n]))
        elif n==2:
            up_compt_sport3.config(text=str(n_upgrade[n]))
        elif n==3:
            up_compt_sport4.config(text=str(n_upgrade[n]))
        elif n==4:
            up_compt_sport5.config(text=str(n_upgrade[n]))
        elif n==5:
            up_compt_sport6.config(text=str(n_upgrade[n]))
        elif n==6:
            up_compt_sport7.config(text=str(n_upgrade[n]))
        elif n==7:
            up_compt_sport8.config(text=str(n_upgrade[n]))
        elif n==8:
            up_compt_sport9.config(text=str(n_upgrade[n]))
        elif n==9:
            up_compt_sport10.config(text=str(n_upgrade[n])) 

def cost(n):
    """ fonction permetant de vérifier que boucle_inf'on a assez de mascotte(monaie du jeu) pour acheter une amélioration. Si oui, enlève le montant de boucle_inf'amélioration à la variable ressource."""
    global ressource
    global upgrade_cost
    if ressource>=upgrade_cost[n]:
        ressource+=-upgrade_cost[n]
        cost_inflation(n)
        nb_mascotte.config(text=str(ressource))
        return True
    return False

def cost_inflation(n):
    """ fonction permetant de faire augmenter le prix d'une amélioration à chaque fois qu'elle est achetée."""
    global upgrade_cost
    upgrade_cost[n]=int(upgrade_cost[n]*1.15)
    if n==0:
        bouton_sport1.config(text=str(upgrade_cost[n]))
    elif n==1:
        bouton_sport2.config(text=str(upgrade_cost[n]))
    elif n==2:
        bouton_sport3.config(text=str(upgrade_cost[n]))
    elif n==3:
        bouton_sport4.config(text=str(upgrade_cost[n]))
    elif n==4:
        bouton_sport5.config(text=str(upgrade_cost[n]))
    elif n==5:
        bouton_sport6.config(text=str(upgrade_cost[n]))
    elif n==6:
        bouton_sport7.config(text=str(upgrade_cost[n]))
    elif n==7:
        bouton_sport8.config(text=str(upgrade_cost[n]))
    elif n==8:
        bouton_sport9.config(text=str(upgrade_cost[n]))
    elif n==9:
        bouton_sport10.config(text=str(upgrade_cost[n]))

def mdp_correct():
    global mdp_admin
    global admin
    if mdp.get()=="Paris 2024":
        admin=True
        bouton_admin.config(text=str("Gagner 1 000 000 000"))

def cheat():
    """fonction permettant que si admin est actif, ajoute 1 milliard à la variable ressource"""
    global ressource
    if admin==True:
        ressource+=1000000000
        nb_mascotte.config(text=str(ressource))
main_window = Tk()
main_window.title("Clickletic")
main_window.geometry("1000x1000")
main_window.configure(background="black")

#Left_shop
left_shop=Frame(main_window, width=250, height=800, bg="black" )
left_shop.place(x='0', y='0')



sport1=Frame(left_shop, width=240, height=155 )
sport1.place(x='5',y='5')

image_sport1=PhotoImage(file="./images/RUGBY.png")

photo_sport1=Label(sport1, width=100, height=100, bg="black", image=image_sport1)
photo_sport1.place(x='15', y='25')

sport1_name=Label(sport1, text="Rugby",)
sport1_name.place(x='150', y='25')

bouton_sport1=Button(sport1, text=str(upgrade_cost[0]), font=15, command=lambda:buy_upgrade(0), width=10)
bouton_sport1.place(x='130' ,y='100')

up_compt_sport1=Label(sport1, text="0", font=15)
up_compt_sport1.place(x='174', y='65')


sport2=Frame(left_shop, width=240, height=155 ,)
sport2.place(x='5',y='165')

image_sport2=PhotoImage(file="./images/ESCRIME.png")

photo_sport2=Label(sport2, width=100, height=100, bg="black", image=image_sport2)
photo_sport2.place(x='15', y='25')

sport2_name=Label(sport2, text="Escrime")
sport2_name.place(x='150', y='25')

bouton_sport2=Button(sport2, text=str(upgrade_cost[1]), font=15, command=lambda:buy_upgrade(1), width=10)
bouton_sport2.place(x='130' ,y='100')

up_compt_sport2=Label(sport2, text="0", font=15)
up_compt_sport2.place(x='174', y='65')


sport3=Frame(left_shop, width=240, height=155 )
sport3.place(x='5',y='325')

image_sport3=PhotoImage(file="./images/BOXE.png")

photo_sport3=Label(sport3, width=100, height=100, bg="black", image=image_sport3)
photo_sport3.place(x='15', y='25')

sport3_name=Label(sport3, text="Boxe",)
sport3_name.place(x='150', y='25')

bouton_sport3=Button(sport3, text=str(upgrade_cost[2]), font=15, command=lambda:buy_upgrade(2), width=10)
bouton_sport3.place(x='130' ,y='100')

up_compt_sport3=Label(sport3, text="0", font=15)
up_compt_sport3.place(x='174', y='65')


sport4=Frame(left_shop, width=240, height=155 )
sport4.place(x='5',y='485')

image_sport4=PhotoImage(file="./images/TENNIS.png")

photo_sport4=Label(sport4, width=100, height=100, bg="black", image=image_sport4)
photo_sport4.place(x='15', y='25')

sport4_name=Label(sport4, text="Tennis")
sport4_name.place(x='150', y='25')

bouton_sport4=Button(sport4, text=str(upgrade_cost[3]), font=15, command=lambda:buy_upgrade(3), width=10)
bouton_sport4.place(x='130' ,y='100')

up_compt_sport4=Label(sport4, text="0", font=15)
up_compt_sport4.place(x='174', y='65')


sport5=Frame(left_shop, width=240, height=155 )
sport5.place(x='5',y='645')

image_sport5=PhotoImage(file="./images/CYCLISME.png")

photo_sport5=Label(sport5, width=100, height=100, bg="black", image=image_sport5)
photo_sport5.place(x='15', y='25')

sport5_name=Label(sport5, text="Cyclisme")
sport5_name.place(x='150', y='25')

bouton_sport5=Button(sport5, text=str(upgrade_cost[4]), font=15, command=lambda:buy_upgrade(4), width=10)
bouton_sport5.place(x='130' ,y='100')

up_compt_sport5=Label(sport5, text="0", font=15)
up_compt_sport5.place(x='174', y='65')


#Right_shop
right_shop=Frame(main_window, width=250, height=800, bg="black")
right_shop.place(x='750', y='0')




sport6=Frame(right_shop, width=240, height=155, )
sport6.place(x='5',y='5')

image_sport6=PhotoImage(file="./images/JUDO.png")

photo_sport6=Label(sport6, width=100, height=100, bg="black", image=image_sport6)
photo_sport6.place(x='15', y='25')

sport6_name=Label(sport6, text="Judo", )
sport6_name.place(x='150', y='25')

bouton_sport6=Button(sport6, text=str(upgrade_cost[5]), font=15, command=lambda:buy_upgrade(5), width=10)
bouton_sport6.place(x='130' ,y='100')

up_compt_sport6=Label(sport6, text="0", font=15)
up_compt_sport6.place(x='174', y='65')


sport7=Frame(right_shop, width=240, height=155,)
sport7.place(x='5',y='165')

image_sport7=PhotoImage(file="./images/GYMNASTIQUE-ARTISTIQUE.png")

photo_sport7=Label(sport7, width=100, height=100, bg="black", image=image_sport7)
photo_sport7.place(x='15', y='25')

sport7_name=Label(sport7, text="Gym")
sport7_name.place(x='150', y='25')

bouton_sport7=Button(sport7, text=str(upgrade_cost[6]), font=15, command=lambda:buy_upgrade(6), width=10)
bouton_sport7.place(x='130' ,y='100')

up_compt_sport7=Label(sport7, text="0", font=15)
up_compt_sport7.place(x='174', y='65')


sport8=Frame(right_shop, width=240, height=155,)
sport8.place(x='5',y='325')

image_sport8=PhotoImage(file="./images/HANDBALL.png")

photo_sport8=Label(sport8, width=100, height=100, bg="black", image=image_sport8)
photo_sport8.place(x='15', y='25')

sport8_name=Label(sport8, text="Handball")
sport8_name.place(x='150', y='25')

bouton_sport8=Button(sport8, text=str(upgrade_cost[7]), font=15, command=lambda:buy_upgrade(7), width=10)
bouton_sport8.place(x='130' ,y='100')

up_compt_sport8=Label(sport8, text="0", font=15)
up_compt_sport8.place(x='174', y='65')



sport9=Frame(right_shop, width=240, height=155,)
sport9.place(x='5',y='485')

image_sport9=PhotoImage(file="./images/NATATION.png")

photo_sport9=Label(sport9, width=100, height=100, bg="black", image=image_sport9)
photo_sport9.place(x='15', y='25')

sport9_name=Label(sport9, text="Natation")
sport9_name.place(x='150', y='25')

bouton_sport9=Button(sport9, text=str(upgrade_cost[8]), font=15, command=lambda:buy_upgrade(8), width=10)
bouton_sport9.place(x='130' ,y='100')

up_compt_sport9=Label(sport9, text="0", font=15)
up_compt_sport9.place(x='174', y='65')



sport10=Frame(right_shop, width=240, height=155,)
sport10.place(x='5',y='645')

image_sport10=PhotoImage(file="./images/ATHLETISME.png")

photo_sport10=Label(sport10, width=100, height=100, bg="black", image=image_sport10)
photo_sport10.place(x='15', y='25')

sport10_name=Label(sport10, text="Athlétisme")
sport10_name.place(x='150', y='25')

bouton_sport10=Button(sport10, text=str(upgrade_cost[9]), font=15, command=lambda:buy_upgrade(9), width=10)
bouton_sport10.place(x='130' ,y='100')

up_compt_sport10=Label(sport10, text="0", font=15)
up_compt_sport10.place(x='174', y='65')




#Middle
middle=Frame(main_window, width=500, height=500, bg="white" )
middle.place(x='250', y='300')

image_mascotte=PhotoImage(file="./images/mascotte.png")


button_clicker=Button(middle,height=480, width=480, command=click, image=image_mascotte, bg="white", fg="white")
button_clicker.place(x='5', y='5')

#Bottom
bottom=Frame(main_window, width=1000, height=200, bg="black" )
bottom.place(x='0', y='800')

stats=Frame(bottom, width=497, height=190, bg="white" )
stats.place(x="5", y="5")

stats_title=Label(stats, text='Statistiques', width=20, font=("", 25), bg="white")
stats_title.place(x="50", y="5")

ressource_total=Label(stats, text=("Mascottes totales: "+str(ressource)), bg="white", font=("", 25))
ressource_total.place(x='25', y='45')

stat_amélioration=Label(stats, text=("Nombre total d'améliorations: "+ str(nb_amelioration)), bg="white", font=("", 25))
stat_amélioration.place(x='25', y='95')

stat_click=Label(stats, text=("Nombre de clicks: "+str(0)), bg="white", font=("", 25))
stat_click.place(x='25', y='145')

menu=Frame(bottom, width=497, height=190, bg="white")
menu.place(x='506', y='5')

menu_title=Label(menu, text='Menu', width=20, font=("", 25), bg="white")
menu_title.place(x="50", y="5")

pause_play=Button(menu, text="Mettre en pause", width=20, height=5, command=start_stop)
pause_play.place(x='10', y='45')

mdp=StringVar()
mdp_admin=Entry(menu, textvariable=str(mdp), width=30, bg="white")
mdp_admin.place(x="230", y="50")


bouton_entree_mdp=Button(menu, width=7, height=3, text="Valider" , command=mdp_correct)
bouton_entree_mdp.place(x="420", y="35")

bouton_admin=Button(menu, text="mdp Admin necessaire", width=20, height=5, command=cheat)
bouton_admin.place(x="250", y="75")

#Scoreboard
scoreboard=Frame(main_window, width=500, height=300, bg="white" )
scoreboard.place(x='250', y='0')


title_mascotte=Label(scoreboard, text="Mascotte jeux olympique", bg="white", font=("", 25))
title_mascotte.place(x='75', y='50')

nb_mascotte=Label(scoreboard, text="0", bg="white", font=("", 25))
nb_mascotte.place(x='240', y='100')



sub_title_mascotte=Label(scoreboard, text="Mascotte /sec", bg="white", font=("", 25)) 
sub_title_mascotte.place(x='160', y='150')


nb_mascotte_sec=Label(scoreboard, text="0", bg="white", font=("", 25))
nb_mascotte_sec.place(x='240', y='200')

loop()
main_window.mainloop()
